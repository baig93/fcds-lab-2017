/*
 * The Game of Life
 *
 * a cell is born, if it has exactly three neighbours
 * a cell dies of loneliness, if it has less than two neighbours
 * a cell dies of overcrowding, if it has more than three neighbours
 * a cell survives to the next generation, if it does not die of loneliness
 * or overcrowding
 *
 * In this version, a 2D array of ints is used.  A 1 cell is on, a 0 cell is off.
 * The game plays a number of steps (given by the input), printing to the screen each time.  'x' printed
 * means on, space means off.
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

typedef unsigned long cell_t;

#define BITS (sizeof(cell_t) * 8)
#define SIZE(b) ((b*b - 1)/BITS +1)
#define GET(board, b, i, j) ((board[(i*b + j) / BITS] & (1ul << ((i*b + j) % BITS))) !=0)
#define SET(board, b, i, j)   ( board[(i*b + j) / BITS] |= (1ul << ((i*b + j) % BITS)) )
#define UNSET(board, b, i, j) ( board[(i*b + j) / BITS] &= ~(1ul << ((i*b + j) % BITS)) )

cell_t* allocate_board (int size)
{
		return (cell_t*) malloc(sizeof(cell_t)*SIZE(size));
}

void free_board (cell_t* board)
{
	free(board);
}


/* return the number of on cells adjacent to the i,j cell */
int adjacent_to (cell_t* board, int size, int i, int j) {
	int	k, l, count=0;

	int sk = (i>0) ? i-1 : i;
	int ek = (i+1 < size) ? i+1 : i;
	int sl = (j>0) ? j-1 : j;
        int el = (j+1 < size) ? j+1 : j;

	for (k=sk; k<=ek; k++)
		for (l=sl; l<=el; l++)
			count+=GET(board, size, k, l);
	count-=GET(board, size, i, l);

	return count;
}

void play (cell_t* board, cell_t* newboard, int size, int thrds)
{
	#pragma omp parallel for schedule(dynamic, 100) num_threads(thrds)
	//int	i, j, a;
	/* for each cell, apply the rules of Life */
	for (int i=0; i<size; i++)
		for (int j=0; j<size; j++)
		{
			int a = adjacent_to (board, size, i, j);
			if(a==2)
			{
				if(GET(board, size, i, j))
					SET(newboard, size, i, j);
				else
					UNSET(newboard, size, i, j);;
			}
			else
			{
				if(a==3)
				{
					SET(newboard, size, i, j);
				}
				else
				{
					UNSET(newboard, size, i, j);
				}
			}
		}
}

/* print the life board */
void print (cell_t* board, int size) {
	int	i, j;
	/* for each row */
	for (j=0; j<size; j++) {
		/* print each column position... */
		for (i=0; i<size; i++)
			printf ("%c", GET(board, size, i, j) ? 'x' : ' ');
		/* followed by a carriage return */
		printf ("\n");
	}
}

/* read a file into the life board */
void read_file (FILE * f, cell_t* board, int size) {
	int	i, j;
	char	*s = (char *) malloc(size+10);
	//char c;
	for (j=0; j<size; j++) {
		/* get a string */
		fgets (s, size+10,f);
		/* copy the string to the life board */
		for (i=0; i<size; i++)
		{
		 	//c=fgetc(f);
			//putchar(c);
			if(s[i]=='x')
				SET(board, size, i, j);
			else
				UNSET(board, size, i, j);
			//board[i][j] = s[i] == 'x';
		}
		//fscanf(f,"\n");
	}
}

int main () {
	int size, steps;

	int thrds=1;
	if(getenv("MAX_CPUS"))
		thrds=atoi(getenv("MAX_CPUS"));
	if(thrds<1)
		thrds=1;

	FILE    *f;
	f = stdin;

	fscanf(f,"%d %d", &size, &steps);
	while (fgetc(f) != '\n') /* no-op */;

	cell_t* prev = allocate_board (size);
	read_file (f, prev,size);
	fclose(f);

	cell_t* next = allocate_board (size);
	cell_t* tmp;

	int i,j;
	#ifdef DEBUG
	printf("Initial \n");
	print(prev,size);
	printf("----------\n");
	#endif

	for (i=0; i<steps; i++)
	{
		play (prev,next,size,thrds);
    #ifdef DEBUG
		printf("%d ----------\n", i);
		print (next,size);
		#endif
		tmp = next;
		next = prev;
		prev = tmp;
	}
	print (prev,size);
	free_board(prev);
	free_board(next);
}
